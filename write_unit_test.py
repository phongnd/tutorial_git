import unittest
from mysum import Sum
import numpy as np

#
# def test_sum(n):
#     assert sum(np.arange(1,n)) == Sum(n), "your Sum is False"
#
#
# def test_sum_1(n):
#     assert sum(np.arange(1,n)) != Sum(n), "your Compare is False"


class Test_sum(unittest.TestCase):

    def test_sum_(self):
        list_n = np.arange(1, 10)
        self.assertEqual(sum(list_n), Sum(list_n), "your Sum is False")

    def test_sum_1(self):
        list_n = np.arange(1, 10)
        self.assertNotEqual(sum(list_n), Sum(list_n), "your Compare is False")


if __name__ == '__main__':
    # test_sum(10)
    # test_sum_1(10)
    unittest.main()
    print("complete your program")